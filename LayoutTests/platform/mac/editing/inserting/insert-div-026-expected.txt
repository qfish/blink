EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutBlockFlow {HTML} at (0,0) size 800x600
    LayoutBlockFlow {BODY} at (8,8) size 784x584
      LayoutBlockFlow {DIV} at (0,0) size 784x212 [border: (2px solid #0000FF)]
        LayoutBlockFlow {DIV} at (14,14) size 756x112
          LayoutText {#text} at (0,0) size 68x28
            text run at (0,0) width 68: "Tests: "
          LayoutBR {BR} at (0,0) size 0x0
          LayoutText {#text} at (0,28) size 716x56
            text run at (0,28) width 716: "Inserting blocks for paragraphs should do a better job of finding a block to"
            text run at (0,56) width 116: "insert after. "
          LayoutInline {A} at (0,0) size 257x28 [color=#0000EE]
            LayoutText {#text} at (115,56) size 257x28
              text run at (115,56) width 257: "<rdar://problem/3996605>"
          LayoutText {#text} at (371,56) size 727x56
            text run at (371,56) width 356: " Insert paragraph command puts new"
            text run at (0,84) width 548: "block in wrong place, creating difficult-to-handle HTML"
        LayoutBlockFlow {DIV} at (14,142) size 756x56
          LayoutText {#text} at (0,0) size 190x28
            text run at (0,0) width 190: "Expected Results: "
          LayoutBR {BR} at (189,22) size 1x0
          LayoutText {#text} at (0,28) size 438x28
            text run at (0,28) width 438: "Should see this content in the red box below: "
          LayoutInline {B} at (0,0) size 21x28
            LayoutText {#text} at (437,28) size 21x28
              text run at (437,28) width 21: "fo"
          LayoutText {#text} at (457,28) size 13x28
            text run at (457,28) width 13: "x"
      LayoutBlockFlow {DIV} at (0,236) size 784x32
        LayoutBlockFlow {DIV} at (0,0) size 784x32 [border: (2px solid #FF0000)]
          LayoutInline {B} at (0,0) size 20x28
            LayoutText {#text} at (2,2) size 20x28
              text run at (2,2) width 20: "fo"
          LayoutText {#text} at (21,2) size 13x28
            text run at (21,2) width 13: "x"
caret: position 3 of child 0 {#text} of child 0 {B} of child 1 {DIV} of child 3 {DIV} of body
