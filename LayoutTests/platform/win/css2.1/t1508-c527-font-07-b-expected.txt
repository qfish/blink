layer at (0,0) size 800x600 scrollHeight 708
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x708 backgroundClip at (0,0) size 800x600 clip at (0,0) size 800x600 outlineClip at (0,0) size 800x600
  LayoutBlockFlow {HTML} at (0,0) size 800x708
    LayoutBlockFlow {BODY} at (8,18) size 784x672 [color=#000080]
      LayoutBlockFlow {P} at (0,0) size 784x672
        LayoutText {#text} at (0,38) size 735x116
          text run at (0,38) width 735: "This text should be 18px sans-serif, in small-caps and italicized. The lines should be one inch"
          text run at (0,134) width 55: "apart. "
        LayoutInline {SPAN} at (0,0) size 778x500 [color=#C0C0C0]
          LayoutText {#text} at (54,134) size 778x500
            text run at (54,134) width 508: "dummy text dummy text dummy text dummy text dummy text dummy "
            text run at (561,134) width 217: "text dummy text dummy text"
            text run at (0,230) width 326: "dummy text dummy text dummy text dummy "
            text run at (325,230) width 451: "text dummy text dummy text dummy text dummy text dummy"
            text run at (0,326) width 92: "text dummy "
            text run at (91,326) width 547: "text dummy text dummy text dummy text dummy text dummy text dummy "
            text run at (637,326) width 126: "text dummy text"
            text run at (0,422) width 417: "dummy text dummy text dummy text dummy text dummy "
            text run at (416,422) width 360: "text dummy text dummy text dummy text dummy"
            text run at (0,518) width 183: "text dummy text dummy "
            text run at (182,518) width 547: "text dummy text dummy text dummy text dummy text dummy text dummy "
            text run at (728,518) width 35: "text"
            text run at (0,614) width 451: "dummy text dummy text dummy text dummy text dummy text"
        LayoutText {#text} at (0,0) size 0x0
