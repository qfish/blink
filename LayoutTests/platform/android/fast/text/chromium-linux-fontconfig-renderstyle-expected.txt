layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutBlockFlow {HTML} at (0,0) size 800x600
    LayoutBlockFlow {BODY} at (8,8) size 784x576
      LayoutBlockFlow {P} at (0,0) size 784x40
        LayoutText {#text} at (0,0) size 232x19
          text run at (0,0) width 232: "This test requires Chromium Linux "
        LayoutInline {TT} at (0,0) size 80x16
          LayoutText {#text} at (232,3) size 80x16
            text run at (232,3) width 80: "test_shell"
        LayoutText {#text} at (312,0) size 20x19
          text run at (312,0) width 20: " in "
        LayoutInline {TT} at (0,0) size 104x16
          LayoutText {#text} at (332,3) size 104x16
            text run at (332,3) width 104: "--layout-test"
        LayoutText {#text} at (436,0) size 782x39
          text run at (436,0) width 334: " mode, as that forces some rendering settings used "
          text run at (770,0) width 12: "in"
          text run at (0,20) width 126: "the following tests."
      LayoutBlockFlow {OL} at (0,56) size 784x473
        LayoutListItem {LI} at (40,0) size 744x55
          LayoutBlockFlow {P} at (0,0) size 744x20
            LayoutListMarker (anonymous) at (-20,0) size 15x19: "1"
            LayoutText {#text} at (0,0) size 358x19
              text run at (0,0) width 358: "The following text should render without anti-aliasing:"
          LayoutBlockFlow {P} at (0,36) size 744x19
            LayoutText {#text} at (0,0) size 157x18
              text run at (0,0) width 157: "Non anti-aliased sans."
        LayoutListItem {LI} at (40,71) size 744x75
          LayoutBlockFlow {P} at (0,0) size 744x40
            LayoutListMarker (anonymous) at (-20,0) size 15x19: "2"
            LayoutText {#text} at (0,0) size 732x39
              text run at (0,0) width 353: "The following text should be slightly-hinted Georgia. "
              text run at (353,0) width 60: "The dots "
              text run at (413,0) width 319: "should be equally spaced, and letters in the word"
              text run at (0,20) width 94: "\"government\" "
              text run at (94,20) width 426: "should be naturally spaced (without an ugly space before the \"e\")."
          LayoutBlockFlow {P} at (0,56) size 744x19
            LayoutText {#text} at (0,0) size 248x18
              text run at (0,0) width 248: "government ................................"
        LayoutListItem {LI} at (40,162) size 744x75
          LayoutBlockFlow {P} at (0,0) size 744x40
            LayoutListMarker (anonymous) at (-20,0) size 15x19: "3"
            LayoutText {#text} at (0,0) size 743x39
              text run at (0,0) width 412: "The following text should be unhinted Verdana. The fontconfig "
              text run at (412,0) width 331: "configuration for this is contradictory, setting both"
              text run at (0,20) width 79: "full-hinting "
              text run at (79,20) width 269: "and no-hinting. The latter should win out."
          LayoutBlockFlow {P} at (0,56) size 744x19
            LayoutText {#text} at (0,0) size 550x18
              text run at (0,0) width 386: "Here is Tigger doing what tiggers do best \x{2026} operating "
              text run at (386,0) width 164: "hydraulic exoskeletons."
        LayoutListItem {LI} at (40,253) size 744x56
          LayoutBlockFlow {P} at (0,0) size 744x20
            LayoutListMarker (anonymous) at (-20,0) size 15x19: "4"
            LayoutText {#text} at (0,0) size 653x19
              text run at (0,0) width 410: "The following text should show a difference caused by forcing "
              text run at (410,0) width 243: "autohinting. Note: the effect is subtle."
          LayoutBlockFlow {P} at (0,36) size 744x20
            LayoutInline {SPAN} at (0,0) size 50x13
              LayoutText {#text} at (0,5) size 50x13
                text run at (0,5) width 50: "autohinted"
            LayoutText {#text} at (50,0) size 4x19
              text run at (50,0) width 4: " "
            LayoutInline {I} at (0,0) size 13x19
              LayoutText {#text} at (54,0) size 13x19
                text run at (54,0) width 13: "vs"
            LayoutText {#text} at (67,0) size 4x19
              text run at (67,0) width 4: " "
            LayoutInline {SPAN} at (0,0) size 67x13
              LayoutText {#text} at (71,5) size 67x13
                text run at (71,5) width 67: "not-autohinted"
        LayoutListItem {LI} at (40,325) size 744x76
          LayoutBlockFlow {P} at (0,0) size 744x40
            LayoutListMarker (anonymous) at (-20,0) size 15x19: "5"
            LayoutText {#text} at (0,0) size 567x19
              text run at (0,0) width 567: "The following text should be the same. It verifies that, given the contradictory settings "
            LayoutInline {TT} at (0,0) size 64x16
              LayoutText {#text} at (567,3) size 64x16
                text run at (567,3) width 64: "hintfull"
            LayoutText {#text} at (631,0) size 31x19
              text run at (631,0) width 31: " and "
            LayoutInline {TT} at (0,0) size 64x16
              LayoutText {#text} at (662,3) size 64x16
                text run at (662,3) width 64: "autohint"
            LayoutText {#text} at (726,0) size 731x39
              text run at (726,0) width 5: ","
              text run at (0,20) width 123: "the latter wins out:"
          LayoutBlockFlow {P} at (0,56) size 744x20
            LayoutInline {SPAN} at (0,0) size 50x13
              LayoutText {#text} at (0,5) size 50x13
                text run at (0,5) width 50: "autohinted"
            LayoutText {#text} at (50,0) size 4x19
              text run at (50,0) width 4: " "
            LayoutInline {I} at (0,0) size 13x19
              LayoutText {#text} at (54,0) size 13x19
                text run at (54,0) width 13: "vs"
            LayoutText {#text} at (67,0) size 4x19
              text run at (67,0) width 4: " "
            LayoutInline {SPAN} at (0,0) size 96x13
              LayoutText {#text} at (71,5) size 96x13
                text run at (71,5) width 96: "hopefully autohinted"
        LayoutListItem {LI} at (40,417) size 744x56
          LayoutBlockFlow {P} at (0,0) size 744x20
            LayoutListMarker (anonymous) at (-20,0) size 15x19: "6"
            LayoutText {#text} at (0,0) size 647x19
              text run at (0,0) width 454: "The following text should show that fontconfig can be used to enable "
              text run at (454,0) width 193: "or disable subpixel rendering."
          LayoutBlockFlow {P} at (0,36) size 744x20
            LayoutInline {SPAN} at (0,0) size 57x18
              LayoutText {#text} at (0,0) size 57x18
                text run at (0,0) width 57: "subpixel"
            LayoutText {#text} at (57,0) size 4x19
              text run at (57,0) width 4: " "
            LayoutInline {I} at (0,0) size 13x19
              LayoutText {#text} at (61,0) size 13x19
                text run at (61,0) width 13: "vs"
            LayoutText {#text} at (74,0) size 4x19
              text run at (74,0) width 4: " "
            LayoutInline {SPAN} at (0,0) size 78x18
              LayoutText {#text} at (78,0) size 78x18
                text run at (78,0) width 78: "no subpixel"
