
SVG SMIL:
PASS: stitchTiles from [noStitch] to [stitch] was [noStitch] at 0
PASS: stitchTiles from [noStitch] to [stitch] was [noStitch] at 0.2
PASS: stitchTiles from [noStitch] to [stitch] was [stitch] at 0.6
PASS: stitchTiles from [noStitch] to [stitch] was [stitch] at 1
PASS: stitchTiles from [stitch] to [noStitch] was [stitch] at 0
PASS: stitchTiles from [stitch] to [noStitch] was [stitch] at 0.2
PASS: stitchTiles from [stitch] to [noStitch] was [noStitch] at 0.6
PASS: stitchTiles from [stitch] to [noStitch] was [noStitch] at 1

Web Animations API:
PASS: stitchTiles from [noStitch] to [stitch] was [noStitch] at -0.4
PASS: stitchTiles from [noStitch] to [stitch] was [noStitch] at 0
PASS: stitchTiles from [noStitch] to [stitch] was [noStitch] at 0.2
PASS: stitchTiles from [noStitch] to [stitch] was [stitch] at 0.6
PASS: stitchTiles from [noStitch] to [stitch] was [stitch] at 1
PASS: stitchTiles from [noStitch] to [stitch] was [stitch] at 1.4
PASS: stitchTiles from [stitch] to [noStitch] was [stitch] at -0.4
PASS: stitchTiles from [stitch] to [noStitch] was [stitch] at 0
PASS: stitchTiles from [stitch] to [noStitch] was [stitch] at 0.2
PASS: stitchTiles from [stitch] to [noStitch] was [noStitch] at 0.6
PASS: stitchTiles from [stitch] to [noStitch] was [noStitch] at 1
PASS: stitchTiles from [stitch] to [noStitch] was [noStitch] at 1.4

