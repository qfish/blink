
SVG SMIL:
PASS: fx from [1] to [6] was [1] at 0
PASS: fx from [1] to [6] was [2] at 0.2
PASS: fx from [1] to [6] was [4] at 0.6
PASS: fx from [1] to [6] was [6] at 1
PASS: fy from [1] to [6] was [1] at 0
PASS: fy from [1] to [6] was [2] at 0.2
PASS: fy from [1] to [6] was [4] at 0.6
PASS: fy from [1] to [6] was [6] at 1

Web Animations API:
PASS: fx from [1] to [6] was [-1] at -0.4
PASS: fx from [1] to [6] was [1] at 0
PASS: fx from [1] to [6] was [2] at 0.2
PASS: fx from [1] to [6] was [4] at 0.6
PASS: fx from [1] to [6] was [6] at 1
PASS: fx from [1] to [6] was [8] at 1.4
PASS: fy from [1] to [6] was [-1] at -0.4
PASS: fy from [1] to [6] was [1] at 0
PASS: fy from [1] to [6] was [2] at 0.2
PASS: fy from [1] to [6] was [4] at 0.6
PASS: fy from [1] to [6] was [6] at 1
PASS: fy from [1] to [6] was [8] at 1.4

