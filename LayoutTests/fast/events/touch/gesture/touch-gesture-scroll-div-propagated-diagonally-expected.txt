This tests that a gesture scroll is propagated from an inner div to an outer div when the inner div has remaining scroll offset on one axis, but not on the other.

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS successfullyParsed is true

TEST COMPLETE
PASS horizontal.scrollLeft is 15
PASS vertical.scrollTop is 20

